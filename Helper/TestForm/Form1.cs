﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Helper.GUI;
using Helper.Threading;
using Helper.Filestructure;
using System.IO;

namespace TestForm
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            this.AlignMultipleControls(button1, button2);
            groupBox1.AlignMultipleControls(button4, button3);

            List<Control> Test = this.GetAllControls(groupBox1);



            this.DoForEveryControl(groupBox1, RenameControl); 
        }

        private bool RenameControl(Control C)
        {
            C.Text = "Test";
            //C.SetWidthByTextLenght();
            C.Enabled = false;
            
            return true;
        }
    }
}
