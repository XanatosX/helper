﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;

namespace Helper.GUI
{
    public static class GUIHelper
    {
        public static void Center(this Control ObjectToCenter, Control ObjectToCenterIn)
        {
            int ContainerCenter = ObjectToCenterIn.Width / 2;
            ObjectToCenter.Location = new Point(ContainerCenter - (ObjectToCenter.Width / 2), ObjectToCenter.Location.Y);
        }

        public static void AlignMultipleControls(this Control ObjectToCenterIn, params Control[] ObjectsToAlign)
        {
            if (ObjectsToAlign.Length == 0)
                return;
            int YPos = ObjectsToAlign[0].Location.Y;

            int StepSize = ObjectToCenterIn.Width / (ObjectsToAlign.Length + 1);
            
            for (int i = 0; i < ObjectsToAlign.Length; i++)
            {
                int CurMultiplier = i + 1;
                Control CurrentControl = ObjectsToAlign[i];
                CurrentControl.Location = new Point((StepSize * CurMultiplier) - (CurrentControl.Width / 2), YPos);
            }
        }

        public static void SetWidthByTextLenght(params Control[] controlsToEdit)
        {
            if (controlsToEdit.Length == 0)
                return;
            for (int i = 0; i < controlsToEdit.Length; i++)
            {
                controlsToEdit[i].SetWidthByTextLenght();
            }
        }

        public static void SetWidthByTextLenght(this Control controlToEdit)
        {
            int TextWidth = TextRenderer.MeasureText(controlToEdit.Text, controlToEdit.Font).Width;
            controlToEdit.Width = 16 + TextWidth;
        }

        public static List<Control> GetAllControls(this Control ControlToSearchFor)
        {
            List<Control> controlList = new List<Control>();
            foreach (Control c in ControlToSearchFor.Controls)
            {

                controlList.Add(c);
                if (c.Controls.Count > 0)
                    controlList.AddRange(c.GetAllControls());
            }
            return controlList;
        }
        public static List<Control> GetAllControls(this Control ControlToSearchFor, Control Filter)
        {
            return ControlToSearchFor.GetAllControls(Filter.GetType());
        }
        public static List<Control> GetAllControls(this Control ControlToSearchFor, Type Filter)
        {
            List<Control> controlList = new List<Control>();
            List<Control> AllcontrolList = ControlToSearchFor.GetAllControls();
            foreach (Control c in AllcontrolList)
            {

                if (c.GetType() == Filter)
                {
                    controlList.Add(c);
                }
            }

            return controlList;
        }

        public static bool DoForEveryControl(this Control Basic, Control Filter, Func<Control, bool> JobToDo)
        {
            return Basic.DoForEveryControl(Filter.GetType(), JobToDo);
        }
        public static bool DoForEveryControl(this Control Basic, Type Filter, Func<Control, bool> JobToDo)
        {
            List<Control> AllcontrolList = Basic.GetAllControls(Filter);
            bool OverallSuccesss = true;
            foreach (Control c in AllcontrolList)
            {
               bool success = JobToDo(c);
               if (!success)
                   OverallSuccesss = false;
            }
            return OverallSuccesss;
        }
        public static bool DoForEveryControl(this Control Basic, Func<Control, bool> JobToDo)
        {
            List<Control> AllcontrolList = Basic.GetAllControls();
            bool OverallSuccesss = true;
            foreach (Control c in AllcontrolList)
            {
                bool success = JobToDo(c);
                if (!success)
                    OverallSuccesss = false;
            }
            return OverallSuccesss;
        }


    }
}
