using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Helper
{
    public static class BasicHelper
    {
      private static Random rnd = new Random();
      
      public static T GetRandom<T>(this T[] arrayBase)
      {
        if (arrayBase.Length == 0)
          return default(T);
        return arrayBase[rnd.Next(0, arrayBase.Length)];
        
      }
      
      public static T GetRandom<T>(this List<T> listBase)
      {
        if (listBase.Count == 0)
          return default(T);
        return listBase[rnd.Next(0, listBase.Count)];
      }
      
    }
}
