﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Helper.Filestructure
{
    public static class Filestructure
    {
        public static string[] GetAllFilesFromFolder(this string Root)
        {
            List<string> AllFiles = new List<string>();

            string[] SubFolder = Directory.GetDirectories(Root);
            if (SubFolder.Length > 0)
            {
                for (int i = 0; i < SubFolder.Length; i++)
                {
                    AllFiles.AddRange(SubFolder[i].GetAllFilesFromFolder());
                }

            }

            AllFiles.AddRange(Directory.GetFiles(Root));
            return AllFiles.ToArray();
        }
    }
}
