﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace Helper.Threading
{
    public class ThreadData
    {
        private Action _actionToPerforme;
        private ManualResetEvent _currentEvent;
        public ManualResetEvent ResetEvent
        {
            get
            {
                return _currentEvent;
            }
        }
        private Action _wrapper;
        public Action Wrapper
        {
            get
            {
                return _wrapper;
            }
        }



        public ThreadData(Action actionToPerform)
        {
            _actionToPerforme = actionToPerform;
            _currentEvent = new ManualResetEvent(false);
            _wrapper = CreateWrapperFunction();
        }

        private Action CreateWrapperFunction()
        {
            Action wrapper = () =>
            {
                try
                {
                    _actionToPerforme();
                }
                finally
                {
                    _currentEvent.Set();
                }
            };
            return wrapper;
        }
    }
}
