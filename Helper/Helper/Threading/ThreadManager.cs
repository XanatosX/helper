﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace Helper.Threading
{
    public class ThreadManager
    {
        List<ThreadData> ActionsToPerform;

        public ThreadManager()
        {
            ActionsToPerform = new List<ThreadData>();
        }

        /// <summary>
        /// Create Action Wrapper to create proper action to perform 
        /// Example:
        /// Action ActionWrapper = () => { Do normal stuff here };                
        /// 
        /// </summary>
        /// <param name="ActionToAdd"></param>
        public void Add(Action ActionToAdd)
        {
            ThreadData tempData = new ThreadData(ActionToAdd);
            ActionsToPerform.Add(tempData);
        }

        public bool Run()
        {
            SplitAndPerforme(ActionsToPerform);
            return true;
        }

        private void SplitAndPerforme(List<ThreadData> DataList)
        {
            if (DataList.Count > 64)
            {
                int NumberOfPools = (int)Math.Ceiling(DataList.Count / 64.0);

                for (int i = 0; i < NumberOfPools; i++)
                {
                    int Start = 64 * i;
                    int End = Start + 64;
                    if (End > DataList.Count)
                        End = DataList.Count;
                    List<ThreadData> TempList = new List<ThreadData>();
                    for (int Icurrent = Start; Icurrent < End; Icurrent++)
                    {
                        int LocalID = Icurrent;
                        TempList.Add(DataList[LocalID]);
                    }
                    SplitAndPerforme(TempList);
                }
            }
            else
            {
                ManualResetEvent[] handlers = new ManualResetEvent[DataList.Count];
                for (int i = 0; i < DataList.Count; i++)
                {
                    int ID = i;
                    handlers[ID] = DataList[ID].ResetEvent;
                    ThreadPool.QueueUserWorkItem(X => DataList[ID].Wrapper());
                }
                WaitHandle.WaitAll(handlers);
            }

        }
    }
}
